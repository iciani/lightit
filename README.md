<div align='center'>
    <img src='https://ingenio.org.uy/wp-content/uploads/2020/09/Captura-de-Pantalla-2021-12-10-a-las-11.13.26.png'/>
    <p>Technical Challenge.</p>

![PHP](https://badgen.net/badge/Php/8.1/blue?)
![Laravel](https://badgen.net/badge/Laravel/10/red?)

</div>

---

## 💾 **ABOUT**

This is the LIGHTIT TECH CHALLENGE.

Files created:

| Type          | Name                                           | Description |
| ------------- | ---------------------------------------------- | --------------------------------------------- |
| Model         | Patient.php                                    |
| Factory       | PatientFactory.php                             |
| Migration     | 2023_08_08_172842_create_patients_table.php    |
| Migration     | 2023_08_09_035336_create_jobs_table.php        |
| Migration     | 2023_08_10_144451_create_failed_jobs_table.php |
| Seeder        | PatientSeeder.php                              |
| Controller    | PatientController.php                          |
| Model         | Patient.php                                    |
| Resource      | PatientResource.php                            | For a better looking JSON RESPONSE and TESTS. |
| Helper        | PatientHelper.php                              | For a better organization.                    |
| Helper        | JasonHelper.php                                | To homogenize the responses.                  |
| Helper        | TestsHelper.php                                | Personal helper that allows us to debug TESTS.|
| Notification  | NewPatientNotification.php                     | Notifications Queue Task.                     |
| Email Template| NewPatientNotification.php                     | Notifications Email Template.                 |

<br />

---

## 🗒️ **INSTALLATION**

### Deployment:

1. clone the repo

```
git clone https://gitlab.com/iciani/lightit.git
```

2. cd into cloned repo

```
cd lightit
```

3. install dependencies

```
composer install
```

4. Remember to Generate .ENV file.

Parameters here are basic. They can be change regarding environment. Remember to set the mailtrap account.

```
cp .env.example .env
```

5. Validate the Code
```
./vendor/bin/phplint
```

```
./vendor/bin/phpcs -s
```

6. Execute Migrations
```
php artisan migrate
```

7. Execute Seeders
```
php artisan db:seed --class=PatientSeeder
```

8. Load POSTMAN collection to TEST the ENDPOINTS
```
LightIt.postman_collection.json
```

<br />

### Run the App:

1. Execute the App in one Terminal

```
php artisan serve
```

2. Execute the Jobs Listener in other Terminal

```
php artisan queue:list
```

3. Finaly Tests can be run into a third terminal to validate 

```
php artisan test
```


<br/>

---

## 🔎 **SHOWCASE**

<img src="https://i.ibb.co/fGrfG7Q/lightit-email-temp.png" alt="lightit-email-temp" border="0">
<br />
<img src="https://i.ibb.co/YBMSfBM/route-list.png" alt="route-list" border="0">
<br />
<img src="https://i.ibb.co/JvD83h2/lightit-tests-pass.png" alt="lightit-tests-pass" border="0">
<br />

---

## 💻 **TECHNOLOGIES**

![PHP](https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white)

![Laravel](https://img.shields.io/badge/laravel-%23FF2D20.svg?style=for-the-badge&logo=laravel&logoColor=white)

![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)

<br />

---