<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 * )
 */
class PatientResource extends JsonResource
{
    /**
     * @OA\Property(format="int64", title="id", default="1", description="id", property="id"),
     * @OA\Property(format="string", description="name", property="name", default="Dummy Name")
     * @OA\Property(format="string", description="email", property="email", default="test@patient.com")
     * @OA\Property(format="string", description="phone", property="phone", default="+5491130425712")
     * @OA\Property(format="string", description="image", property="image")
     * @OA\Property(format="datetime", description="datecreated", property="datecreated", default="null")
     * @OA\Property(format="datetime", description="dateupdated", property="dateupdated", default="null")
     *
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request = null)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'image' => $this->image,
            'datecreated' => optional($this->datecreated)->toDateTimeString(),
            'dateupdated' => optional($this->dateupdated)->toDateTimeString(),
        ];
    }
}
