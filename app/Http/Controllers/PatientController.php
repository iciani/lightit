<?php

namespace App\Http\Controllers;

use App\Helpers\JsonHelper;
use App\Helpers\PatientHelper;
use App\Http\Resources\PatientResource;
use App\Models\Patient;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        try {
            $patients = Patient::all();

            return JsonHelper::success([
                'patients' => PatientResource::collection($patients),
            ]);
        } catch (Exception $e) {
            return JsonHelper::error($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string|max:100',
                'email' => 'required|email|unique:patients,email|max:100',
                'phone' => 'sometimes|string|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|nullable',
                'image' => 'sometimes|max:2048|image|mimes:jpeg,jpg,png,bmp',
            ]);
            $fieldsData = array_merge($request->except('image'), ['image' => PatientHelper::imageStore($request)]);
            $patient = Patient::create($fieldsData);
            PatientHelper::registrationConfirmation($request->email);

            return JsonHelper::success([
                'patient' => new PatientResource($patient),
            ]);
        } catch (Exception $e) {
            return JsonHelper::error($e);
        }
    }
}
