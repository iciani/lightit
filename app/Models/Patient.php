<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    protected $table = 'patients';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'email',
        'phone',
        'image',
    ];

    protected $dates = [
        'datecreated',
        'dateupdated',
    ];

    public const CREATED_AT = 'datecreated';

    public const UPDATED_AT = 'dateupdated';
}
