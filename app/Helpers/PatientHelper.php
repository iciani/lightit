<?php

namespace App\Helpers;

use App\Notifications\NewPatientNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

final class PatientHelper
{
    /**
     * Patiente Helper for Image Storing.
     */
    public static function imageStore(Request $request): string
    {
        $uniqueFileName = "";
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $uniqueFileName = $request->email . '.' . $request->file('image')->extension();
            $request->file('image')->storeAs($uniqueFileName);
        }

        return $uniqueFileName;
    }

    /**
     * Patiente Helper for Registration Confirmation Email.
     *
     * @return bool
     */
    public static function registrationConfirmation(string $email): void
    {
        Notification::route('mail', $email)->notify(new NewPatientNotification());
    }
}
