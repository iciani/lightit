<?php

namespace Tests\Unit\Api\V1;

use App\Helpers\TestsHelper;
use App\Http\Resources\PatientResource;
use App\Models\Patient;
use App\Notifications\NewPatientNotification as NewPatientNotification;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class PatientsTest extends TestCase
{
    public function testGetPatients(): void
    {
        $this->assertEquals(0, Patient::all()->count());
        $cant = 5;
        $patients = Patient::factory($cant)->create();
        $response = $this->get("/api/v1/patients");
        $jsonResponse = json_decode($response->getContent(), true);
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'success' => true,
                'patients' => PatientResource::collection($patients)->toArray(new \Illuminate\Http\Request()),
            ]);
        $this->assertEquals($cant, count($jsonResponse['patients']));
    }

    public function testStorePatient(): void
    {
        Storage::fake();
        Queue::fake();
        Queue::assertNothingPushed();
        Notification::fake();
        Notification::assertNothingSent();
        $this->assertEquals(0, Patient::all()->count());
        $data = [
            'name' => $this->faker->firstName() . " " . $this->faker->lastName(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->numerify('###-###-####'),
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $response = $this->post("/api/v1/patients", $data);
        TestsHelper::dumpApiResponsesWithErrors($response);
        $jsonResponse = json_decode($response->getContent(), true);
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'success' => true
            ]);
        $this->assertEquals(1, Patient::whereEmail($data['email'])->get()->count());
        $this->assertStringContainsString($data['email'], $jsonResponse['patient']['image']);
        unset($data['image']);
        foreach ($data as $key => $value) {
            $this->assertEquals($value, $jsonResponse['patient'][$key], "key {$key}");
        }
        Notification::assertSentTo(new AnonymousNotifiable(), NewPatientNotification::class);
    }
}
