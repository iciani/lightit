<?php

use App\Http\Controllers\PatientController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::group(['prefix' => 'v1', 'as' => 'v1.'], function () {
    Route::apiResource('patients', PatientController::class)->only(['index', 'store']);
});

// Route::get('email-test', function(){
//     $details['email'] = 'ignacio.ciani@lightit.io';
//     dispatch(new App\Jobs\SendEmailJob($details));
//     dump('done');
// });
